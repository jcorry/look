![pipeline](https://gitlab.com/jcorry/look/badges/main/pipeline.svg)
![goreportcard](https://goreportcard.com/badge/gitlab.com/jcorry/look)
# look

## An opinionated structured logger for Go services.

Datadog or some similar indexing service is usually the primary interface for searching, filtering, monitoring and reading log entries. Datadog is awesome! It is also expensive. As such, there’s operational value to eliminating unnecessary logging and reducing both the number of entries and the byte size of entries being sent to Datadog.
There are a number of ways to achieve this ideal with pros and cons to each.
One way would be to simply not log anything. This would reduce our datadog costs to $0. By the measure of “how low did you get the datadog cost?” this would be 100% effective. It would have some negative consequences that would probably also wind up costing a lot more than the savings realized on the Datadog bill.
So any discussion of what to log and why really needs to consider the pros and cons and the true costs of logging/not-logging.
Here are some great resources to look to for guidance around logging (I don’t agree with 100% of everything included here but there is a lot of sensible food for thought):
- https://dave.cheney.net/2015/11/05/lets-talk-about-logging
- https://www.ardanlabs.com/blog/2017/05/design-philosophy-on-logging.html
- https://www.sentinelone.com/blog/the-10-commandments-of-logging/

## How it works
I introduce the look package for logging/metrics gathering. It works differently than
just providing a pointer to a logging struct and allowing holders of that pointer to call one of it’s funcs.
The key difference is that it works with context to preserve call stack data and to allow for linking log events from function calls within a call stack.
A root context is created by HTTP middleware (for the API) and that context contains a pointer to a logger. This requires that each function log it’s own Info and Error entries, but provides some better insights for the developer trying to figure out what happened in a given request. With this approach, the root context created at HTTP request time is given a
correlation ID , each calling function opens a Span from the provided context. `ctx, span := look.OpenSpan(ctx)`
Now, span has a `.Error()` and a `.Info()` function that when called, will add the correlation ID that was in the `ctx` passed to `OpenSpan`. `ctx` now has that correlation ID embedded, so child spans opened this way in called functions will have the same correlation ID.
With this approach, I can get the correlation ID from any error (or info) level log statement and find any errors logged in the
same call stack (ie: HTTP request). This is incredibly useful for debugging/troubleshooting/auditing failures. A typical error payload is 3-4K (a bit high)...but errors are rare and can be made rarer, and we don't have to log the entire call stack, reducing the payload dramatically.
### Errors are logged when and where they are created vs being wrapped and returned
Why? We can just wrap an error and return it and let the top level caller handle the logging. If we did that though, there would be some consequences:
The only data you would have about any given Error entry is the location of where the log event happened. This would be near useless since it would always be in the top level (presentation layer? HTTP middleware?). The error itself would contain string appendages:
`Application layer error: Infrastructure layer error string: Domain layer error string`
Without the call stack data, you don’t really know what happened...and to find out you have to spend a lot of time searching through the codebase for strings. Strings that are arbitrarily used by a developer to wrap an error.
Even if you agree on a standard for error wrapping strings (like, use the function name where the error was encountered), that isn’t precise enough (eg: `GetByGUID` would clearly be standards compliant...but is so generic you’d have no idea where it came from...that function name could belong to many receivers/packages)

## How to use it
1. Import it

> go get -u gitlab.com/jcorry/look

2. Create a context with a logger (I use zap in prod...tests use stdout)
```
l := &look.LeveledLogger{
    Logger: look.StdLogger{Writer: logger, Format: format},
    Level:  level,
    Format: format,
}

ctx := look.CtxWithLogger(context.Background(), l)
```

3. Open a span with that context, log from the span
```
ctx, span := look.OpenSpan(ctx)
defer span.Close()

span.Info("hello!")
```

4. Include fields if you like
```
ctx, span := look.OpenSpan(ctx)
defer span.Close()

f := look.Fields{"user_guid": u.GUID, "email": "user@gitlab.com"}

span.Info("hello!", f)
```

## HTTP Middleware

There is also an http.Handler middleware that will create a log entry for each request/response, generating log lines with this JSON:
```
{
  "level": "info",
  "timestamp": "2022-12-01T10:19:28.038162-05:00",
  "caller": "look@v0.3.0/middleware.go:56",
  "msg": " | INFO | API | 200 | GET | /api/v1/health |",
  "component": "my-project",
  "env": "development",
  "version": "v0.1.0",
  "name": "look.LogMiddleware.func1",
  "correlation_id": "2IJmH3UrJBQo5P74DQrRTAxpy3H",
  "span_id": "2IJmH3Aj15mVjqOxVz5uG4Mnafm",
  "http.method": "GET",
  "http.url": "/api/v1/health",
  "http.url_path": "/api/v1/health",
  "duration": 0.26259200000000005,
  "req": "",
  "resp": "user: jcorry@gmail.com",
  "request_id": "be2IJmGyPdpS3xKJp3fu6pEaLXlQo",
  "http.status_code": 200
}
```

Elsewhere, we will have log entries representing the entire call stack, like this (this is an example from a real projects integration test output):
```
{"lev":"DEBUG","tim":"2023-01-13 18:18:08.435143 -0500 EST m=+0.804785728","nam":"auth.Interceptor","cid":"2KIAnLwFbilxUeqLWKmHjOVljl0","sid":"2KIAnK3FkE2Rp5PN0hzRsyyYUbQ","fil":"/Users/jcorry/Code/chipmunq/go/internal/look/span.go","lin":"63","msg":"span opened"}
{"lev":"INFO","tim":"2023-01-13 18:18:08.435159 -0500 EST m=+0.804802093","nam":"auth.Interceptor","cid":"2KIAnLwFbilxUeqLWKmHjOVljl0","sid":"2KIAnK3FkE2Rp5PN0hzRsyyYUbQ","fil":"/Users/jcorry/Code/chipmunq/go/internal/auth/interceptor.go","lin":"45","msg":"authorizing..."}
{"lev":"DEBUG","tim":"2023-01-13 18:18:08.435176 -0500 EST m=+0.804819001","nam":"auth.Interceptor|auth.authorize","cid":"2KIAnLwFbilxUeqLWKmHjOVljl0","sid":"2KIAnGPVHp7jm81a7ewCxNG8fbi","fil":"/Users/jcorry/Code/chipmunq/go/internal/auth/interceptor.go","lin":"55","msg":"span opened (child)"}
{"lev":"DEBUG","tim":"2023-01-13 18:18:08.435328 -0500 EST m=+0.804970950","nam":"auth.Interceptor|auth.authorize","cid":"2KIAnLwFbilxUeqLWKmHjOVljl0","sid":"2KIAnGPVHp7jm81a7ewCxNG8fbi","fil":"/Users/jcorry/Code/chipmunq/go/internal/auth/interceptor.go","lin":"88","msg":"span closed dur=151866ns"}
{"lev":"DEBUG","tim":"2023-01-13 18:18:08.435369 -0500 EST m=+0.805012181","nam":"auth.Interceptor|auth.authorize|presentation.NewAddContact.func1","cid":"2KIAnLwFbilxUeqLWKmHjOVljl0","sid":"2KIAnIOg2XGq8xrJT1ks1A9Sa5D","fil":"/Users/jcorry/Code/chipmunq/go/services/minibear/presentation/add_contact.go","lin":"22","msg":"span opened (child)"}
{"lev":"DEBUG","tim":"2023-01-13 18:18:08.435401 -0500 EST m=+0.805043906","nam":"auth.Interceptor|auth.authorize|presentation.NewAddContact.func1|application.NewAddContact.func1","cid":"2KIAnLwFbilxUeqLWKmHjOVljl0","sid":"2KIAnMqc9EXEgzWdpN1RTowlGNn","fil":"/Users/jcorry/Code/chipmunq/go/services/minibear/application/add_contact.go","lin":"21","msg":"span opened (child)"}
{"lev":"DEBUG","tim":"2023-01-13 18:18:08.439876 -0500 EST m=+0.809518640","nam":"auth.Interceptor|auth.authorize|presentation.NewAddContact.func1|application.NewAddContact.func1","cid":"2KIAnLwFbilxUeqLWKmHjOVljl0","sid":"2KIAnMqc9EXEgzWdpN1RTowlGNn","fil":"/Users/jcorry/Code/chipmunq/go/services/minibear/application/add_contact.go","lin":"54","msg":"span closed dur=4470995ns"}
{"lev":"DEBUG","tim":"2023-01-13 18:18:08.439905 -0500 EST m=+0.809548002","nam":"auth.Interceptor|auth.authorize|presentation.NewAddContact.func1","cid":"2KIAnLwFbilxUeqLWKmHjOVljl0","sid":"2KIAnIOg2XGq8xrJT1ks1A9Sa5D","fil":"/Users/jcorry/Code/chipmunq/go/services/minibear/presentation/add_contact.go","lin":"46","msg":"span closed dur=4535758ns"}
{"lev":"DEBUG","tim":"2023-01-13 18:18:08.439939 -0500 EST m=+0.809581671","nam":"auth.Interceptor","cid":"2KIAnLwFbilxUeqLWKmHjOVljl0","sid":"2KIAnK3FkE2Rp5PN0hzRsyyYUbQ","fil":"/Users/jcorry/Code/chipmunq/go/internal/auth/interceptor.go","lin":"51","msg":"span closed dur=4794072ns"}
```

In this case, a gRPC interceptor adds the logger to the context and log statements performed in functions down the call stack carry the correlation_id generated at request ingress. All function calls can be correlated to a parent, tying everything that happened (and was logged) in the scope of a request together.

This makes troubleshooting errors much easier than when we just have error strings and the location where those strings were logged.

