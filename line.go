package look

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/go-stack/stack"
)

// LogLine represents a single log line
type LogLine struct {
	Level         string `json:"lev"`
	Timestamp     string `json:"tim"`
	Name          string `json:"nam"`
	CorrelationID string `json:"cid"`
	SpanID        string `json:"sid"`
	File          string `json:"fil"`
	LineNumber    string `json:"lin"`
	Message       string `json:"msg"`
	Error         string `json:"err"`
	Fields        Fields `json:"flds"`
}

// Fields allows arbitrary structured data to be logged.
type Fields map[string]interface{}

func withFields(fields []Fields) Fields {
	f := map[string]interface{}{}
	for _, fieldMap := range fields {
		for key, val := range fieldMap {
			f[key] = val
		}
	}
	return f
}

// NewLine populates a log Line with values and returns it.
func NewLine(lev LogLevel, s *Span, msg string, err error, f Fields, c *stack.Call) *LogLine {
	ll := LogLine{
		Level:         LevelToString(lev),
		Name:          s.name,
		CorrelationID: s.cID,
		SpanID:        s.sID,
		File:          fmt.Sprintf("%#s", c),
		LineNumber:    fmt.Sprintf("%d", c),
		Message:       msg,
		Timestamp:     time.Now().String(),
		Fields:        f,
	}
	if err != nil {
		ll.Error = err.Error()
	}
	return &ll
}

// JSON encodes the log line as JSON for machine-consumption
func (ll LogLine) JSON() string {
	b, err := json.Marshal(ll)
	if err != nil {
		return fmt.Sprintf("error encoding line to JSON: %#v", ll)
	}
	return string(b)
}

// Human encodes the log line as a line & indent format for human-consumption
func (ll LogLine) Human() string {
	return fmt.Sprintf("level=%s time=%s name=%s cid=%s sid=%s file=%s line=%s msg=%s",
		ll.Level, ll.Timestamp, ll.Name, ll.CorrelationID, ll.SpanID, ll.File, ll.LineNumber, ll.Message)
}
