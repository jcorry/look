package looktest

import (
	"bytes"
	"context"
	"log"
	"os"

	"gitlab.com/jcorry/look"
)

// TestableContext returns a context that has a byte buffer logger injected into it
// Useful for capturing test output
func TestableContext(level look.LogLevel, format look.LogFormat) (context.Context, *bytes.Buffer) {
	var buf bytes.Buffer
	logger := log.New(os.Stdout, ``, 0)
	logger.SetOutput(&buf)

	l := &look.LeveledLogger{
		Logger: look.StdLogger{Writer: logger, Format: format},
		Level:  level,
		Format: format,
	}
	return look.CtxWithLogger(context.Background(), l), &buf
}

// StdLogCtx provides a context with the stdio logger
func StdLogCtx(logLevel look.LogLevel) context.Context {
	return look.CtxWithLogger(context.Background(), look.NewStdLogger(logLevel, look.Human))
}
