//go:build !integration
// +build !integration

package look_test

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/jcorry/look"

	"github.com/stretchr/testify/require"

	"gitlab.com/jcorry/look/looktest"
)

func TestLogMiddleware(t *testing.T) {
	tests := []struct {
		name    string
		handler http.Handler
		status  string
	}{
		{
			name: fmt.Sprintf("%d", http.StatusOK),
			handler: http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				_, err := w.Write([]byte("OK"))
				if err != nil {
					w.WriteHeader(http.StatusInternalServerError)
					return
				}
				w.WriteHeader(http.StatusOK)
			}),
			status: "OK",
		},
		{
			name: fmt.Sprintf("%d", http.StatusNotFound),
			handler: http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				_, err := w.Write([]byte("Not Found"))
				if err != nil {
					w.WriteHeader(http.StatusInternalServerError)
					return
				}
				w.WriteHeader(http.StatusNotFound)
			}),
			status: "Not Found",
		},
	}

	for _, tt := range tests {
		ctx, buf := looktest.TestableContext(look.Info, look.JSON)
		req := httptest.NewRequest(http.MethodGet, "https://domain.com/path", nil)
		req = req.WithContext(ctx)

		res := httptest.NewRecorder()
		l, ok := look.LoggerFromCtx(ctx)
		require.True(t, ok)
		h := look.LogMiddleware(l, tt.handler)

		h.ServeHTTP(res, req)

		require.Contains(t, buf.String(), "https://domain.com/path")
		require.Contains(t, buf.String(), tt.status)
	}
}
