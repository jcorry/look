//go:build !integration
// +build !integration

package look_test

import (
	"encoding/json"
	"fmt"
	"strings"
	"testing"

	"github.com/pkg/errors"
	"github.com/stretchr/testify/require"

	"gitlab.com/jcorry/look"
	"gitlab.com/jcorry/look/looktest"
)

func TestSpan_Debug(t *testing.T) {
	logged := RunTestProgram(look.Debug)

	require.Greater(t, len(logged), 0)
	require.Contains(t, logged, "rootSpan")
	require.Contains(t, logged, "testDebug")
	require.Contains(t, logged, "testDebugf")
	require.Contains(t, logged, "testInfo")
	require.Contains(t, logged, "testInfof")
	require.Contains(t, logged, "testError")
	require.Contains(t, logged, "testErrorf")
	require.Contains(t, logged, "testGoErr")
	require.Contains(t, logged, "rootSpan|look_test.RunTestProgram")
	require.Contains(t, logged, "hello!")
	require.Contains(t, logged, "span_test.go")
	require.Contains(t, logged, "opened")

	verifyAllJSON(t, logged)
}

func TestSpan_Info(t *testing.T) {
	logged := RunTestProgram(look.Info)

	require.Greater(t, len(logged), 0)
	require.Contains(t, logged, "rootSpan")
	require.NotContains(t, logged, "testDebug")
	require.NotContains(t, logged, "testDebugf")
	require.Contains(t, logged, "testInfo")
	require.Contains(t, logged, "testInfof")
	require.Contains(t, logged, "testError")
	require.Contains(t, logged, "testErrorf")
	require.Contains(t, logged, "testGoErr")
	require.Contains(t, logged, "rootSpan|look_test.RunTestProgram")
	require.Contains(t, logged, "hello!")
	require.Contains(t, logged, "span_test.go")
	require.NotContains(t, logged, "opened")

	verifyAllJSON(t, logged)
}

func TestSpan_Error(t *testing.T) {
	logged := RunTestProgram(look.Error)

	require.Greater(t, len(logged), 0)
	require.Contains(t, logged, "rootSpan")
	require.NotContains(t, logged, "testDebug")
	require.NotContains(t, logged, "testDebugf")
	require.NotContains(t, logged, "testInfo")
	require.NotContains(t, logged, "testInfof")
	require.Contains(t, logged, "testError")
	require.Contains(t, logged, "testErrorf")
	require.Contains(t, logged, "testGoErr")
	require.NotContains(t, logged, "rootSpan|look_test.RunTestProgram") // the only message in the child was an info level
	require.NotContains(t, logged, "hello!")
	require.Contains(t, logged, "span_test.go")
	require.NotContains(t, logged, "opened")

	verifyAllJSON(t, logged)
}

// RunTestProgram uses a logger and prints a log to the returned string
func RunTestProgram(logLevel look.LogLevel) string {
	ctx, buf := looktest.TestableContext(logLevel, look.JSON)
	ctx, s := look.OpenCustomSpan(ctx, "rootSpan")

	s.Debug("testDebug")
	s.Debugf("%s", "testDebugf")
	s.Info("testInfo")
	s.Infof("%s", "testInfof")
	s.Error(errors.New("testError"))
	s.Errorf(errors.New("testErrorf"), "%s", look.Fields{"testGoErr": ""})

	_, cs := look.OpenSpan(ctx)
	cs.Info("hello!")

	s.Close()
	return buf.String()
}

func verifyAllJSON(t *testing.T, logged string) {
	lines := strings.Split(logged, "\n")
	for _, line := range lines {
		if line == "" {
			continue
		}
		// test each line is valid json
		ll := &look.LogLine{}
		err := json.Unmarshal([]byte(line), ll)
		require.NoError(t, err, "failed to unmarshal json log line %s", line)
	}
}

func TestHumanOutput(t *testing.T) {
	ctx, buf := looktest.TestableContext(look.Error, look.Human)
	_, s := look.OpenSpan(ctx)

	err := externalFunc()
	s.Error(err)

	s.Close()

	result := buf.String()
	require.Contains(t, result, "msg=external context: root cause\n")
	require.Contains(t, result, "/look/span_test.go")
	require.Contains(t, result, "look_test.TestHumanOutput")
}

func externalFunc() error {
	return errors.Wrap(func() error { return fmt.Errorf("root cause") }(), "external context")
}
